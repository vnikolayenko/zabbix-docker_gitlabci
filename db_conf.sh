#!/bin/bash
export MYSQL_USER=${MYSQL_USER:-zabbix}
export MYSQL_PASS=${MYSQL_PASS:-$MYSQL_USER}
export MYSQL_DB=${MYSQL_DB:-$MYSQL_USER}


service mysql start

mysql --user=root mysql <<SQL_CMD
CREATE DATABASE ${MYSQL_DB} CHARACTER SET utf8 COLLATE utf8_bin;
SQL_CMD

mysql --user=root mysql <<SQL_CMD
CREATE USER '${MYSQL_USER}' IDENTIFIED by '${MYSQL_PASS}';
SQL_CMD

mysql --user=root mysql <<SQL_CMD
GRANT ALL PRIVILEGES ON ${MYSQL_DB}.* TO '${MYSQL_USER}';
SQL_CMD

cat ./create.sql | mysql -u${MYSQL_USER} --password=${MYSQL_PASS} ${MYSQL_DB}

echo "DBPassword="$MYSQL_PASS >> /etc/zabbix/zabbix_server.conf

mkdir /run/zabbix
chown -R zabbix:zabbix /var/log/zabbix
chown -R zabbix:zabbix /var/run/zabbix
chmod -R 775 /var/log/zabbix/
chmod -R 775 /var/run/zabbix/

/usr/sbin/zabbix_server --foreground -c /etc/zabbix/zabbix_server.conf
