# Zabbix-Docker_GitlabCI

Docker image with Zabbix Server made by own written Dockerfile based on Ubuntu. Triggered GitlabCI with each push to master for pushing docker image to Docker Hub.

My steps which I tried to do:
1. Set up simple dockerfile with simple gitlab_ci.yml for pushing first image to GitLab Registry.
2. Try to push my image to DockerHub.

    A) Downloaded and installed gitlab-runner_amd64.deb for local WSL(Debian).
    But after that I got an error.
    "systemctl start gitlab-runner
    System has not been booted with systemd as init system (PID 1). Can't operate."
    So I tried to install runner on Windows.
    Download .exe to spesific directory.
        a) Registered runner (executer shell);
        b) Verified runner;
        c) Run runner;

    B) Wrote a new version of gitlab_ci.yaml where image would be build by runner and pushed to Docker Hub.

3. Try to write Dockerfile which would start Zabbix server with mysql DB.
    A) I copied and pasted Dockerfile with docker-entrypoint.sh script from official Zabbix repositiry https://github.com/zabbix/zabbix-docker/tree/5.0/server-mysql/ubuntu. This was done to get something worked like image with Zabbix server on Docker Hub. ( Working commit for this version: https://gitlab.com/vnikolayenko/zabbix-docker_gitlabci/-/tree/da2f9612ad28fc85dfc0386d4cfe9e1d08436366 )

4. Try to write own Dockerfile. I used this manual for creating basis for my "RUN" steps: https://www.zabbix.com/download?zabbix=5.0&os_distribution=ubuntu&os_version=20.04_focal&db=mysql&ws=apache.

Difficulties with which I faced that this file doesn't exist '/usr/share/doc/zabbix-server-mysql*/create.sql.gz' while I try to install all from official packages 'https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+focal_all.deb' . On this forum a lot of people faced with the same problem https://www.zabbix.com/forum/zabbix-help/355372-create-sql-gz-no-such-file-or-directory. 
So, I get this file in such way:
1) Start official container and thanks to command "docker cp" take "create.sql.gz" file to local dir.
2) Than this .sql I use in my own Dockerfile for setuping DB schema. 
3) All other steps ware checked manually from docker container. So, all other was done by my own.

The second difficulties with which I faced is that not all needed files exist by default after installing packages and due to it not all needed dirs have needed permissions. So I created needed user and give needed permissions for dirs which used by Zabbix server.

For settuping MySQL with Zabbix server I use db_conf.sh

So, for starting container using my own image you should use almost the same command as official.
Default env variables MYSQL_USER, MYSQL_PASSWORD and MYSQL_DB = zabbix
``` docker run --name some-zabbix-server-mysql -d vnikolayenko/zabbix_server:latest ```
``` docker exec -ti some-zabbix-server-mysql /bin/bash ```
``` docker logs some-zabbix-server-mysql ```

Now in repo is my final version of Dockerfile. Enjoy!)
What was done:
Gitlab CI with pushing to Docker Hub.
Docker Hub: https://hub.docker.com/repository/docker/vnikolayenko/zabbix_server
Wrote own Dockerfile with additional files( bash script and copy of db schema .sql file) which start MySQL for Zabbix and configured Zabbix Server.